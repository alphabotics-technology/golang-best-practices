/*
 * Http Handle JSON format
 * This example will show you how to encode and decode JSON data
 * and response JSON for client using the "encoding/json" package.
 * Test it:
 * 		$ curl -s -X POST -d'{"firstname":"Quan","lastname":"Vu","Job":"Software Engineer"}' http://localhost:8080/users
 * 		$ curl -s http://localhost:8080/users/1
 */

package main

import (
	"encoding/json"
	"fmt"
	"net/http"
)

type User struct {
	Firstname string `json:"firstname"`
	Lastname  string `json:"lastname"`
	Job       string `json:job`
}

func main() {
	http.HandleFunc("/users", func(w http.ResponseWriter, r *http.Request) {
		var user User
		json.NewDecoder(r.Body).Decode(&user)
		fmt.Fprintf(w, "%s %s is a %s.", user.Firstname, user.Lastname, user.Job)
	})

	http.HandleFunc("/users/1", func(w http.ResponseWriter, r *http.Request) {
		user := User{
			Firstname: "Quan",
			Lastname:  "Vu",
			Job:       "Software Engineer",
		}
		json.NewEncoder(w).Encode(user)
	})

	http.ListenAndServe(":8080", nil)
}
