/**
 * This example will show you how to hash password using bcrypt.
 * Reference: https://godoc.org/golang.org/x/crypto/bcrypt
 * $ go get golang.org/x/crypto/bcrypt
 */
package main

import (
	"fmt"

	"golang.org/x/crypto/bcrypt"
)

var hashLen = 12

func hashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), hashLen)
	return string(bytes), err
}

func checkPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

func main() {
	password := "myP@ssWord"
	hash, _ := hashPassword(password)

	fmt.Println("Password: ", password)
	fmt.Println("Hash: ", hash)

	match := checkPasswordHash(password, hash)
	fmt.Println("Match: ", match)
}
