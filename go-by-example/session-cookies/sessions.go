/*
 * Session & Cookies
 * This example will show you how to store data in session and cookies using gorilla/sessions package.
 * Test on browser:
	- http://localhost:8080/admin
	- http://localhost:8080/login
	- http://localhost:8080/logout

 * Test it with curl:
	$ curl -s http://localhost:8080/admin
	$ curl -s -I http://localhost:8080/login									# Result: Set-Cookie: cookie-name=MTU1ODQz...
	$ curl -s --cookie "cookie-name=MTU1ODQz..." http://localhost:8080/admin	# Access with cookie
	$ curl -s http://localhost:8080/logout
*/

package main

import (
	"fmt"
	"net/http"

	"github.com/gorilla/sessions"
)

// Create a secret key for session, must be 16, 24 or 32 bytes long (AES-128, AES-192, or AES-256)
var secretKey = []byte("my-secrect-key")
var store = sessions.NewCookieStore(secretKey)

func adminpage(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, "cookie-name")
	auth, ok := session.Values["authenticated"].(bool)
	// Check if user is authenticated
	if !ok || !auth {
		http.Error(w, "Fodbidden", http.StatusForbidden)
		return
	}

	// Print welcome message if logged in
	fmt.Fprintln(w, "Welcome to admin page!")
}

func login(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, "cookie-name")
	// Authentication here ...

	// Set session for authentication
	session.Values["authenticated"] = true
	session.Save(r, w)
	fmt.Fprintln(w, "You are logged in!")
}

func logout(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, "cookie-name")
	// Revoke the authentication
	session.Values["authenticated"] = false
	session.Save(r, w)
	fmt.Fprintln(w, "You are logged out!")
}

func main() {
	http.HandleFunc("/admin", adminpage)
	http.HandleFunc("/login", login)
	http.HandleFunc("/logout", logout)

	http.ListenAndServe(":8080", nil)
}
