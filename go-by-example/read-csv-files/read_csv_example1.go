// Read a CSV File into a Struct
// Use Go's Standard Library: “encoding/csv”
// Author: quanvu

package main

import (
	"encoding/csv"
	"fmt"
	"os"
)

type Phone struct {
	PhoneNumber      string
	ActivationDate   string
	DeactivationDate string
}

func main() {

	filename := "phone_list.csv"

	// Open CSV file
	f, err := os.Open(filename)
	if err != nil {
		panic(err)
	}
	defer f.Close()

	// Read File into a Variable
	lines, err := csv.NewReader(f).ReadAll()
	if err != nil {
		panic(err)
	}

	// Loop through lines & turn into the object
	for _, line := range lines {
		data := Phone{
			PhoneNumber:      line[0],
			ActivationDate:   line[1],
			DeactivationDate: line[2],
		}
		fmt.Println(data.PhoneNumber + " " + data.ActivationDate + " " + data.ActivationDate)
	}
}
