# Go Basics

Start learning Golang with the simple code sample.

| TODO                        | DONE |
|-----------------------------|------|
| 01. First program           |      |
| 02. Types                   |      |
| 03. Variables               |      |
| 04. Control Structures      |      |
| 05. Arrays, Slices and Maps |      |
| 06. Funstions               |      |
| 07. Pointers                |      |
| 08. Structs and Interfaces  |      |
| 09. Concurrency             |      |
| 10. Packages                |      |
| 11. Testing                 |      |
| 12. Core-Packages           |      |