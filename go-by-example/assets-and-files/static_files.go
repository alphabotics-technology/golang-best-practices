/* Assets and Files
 * In Golang, we can serve the static files like Css, Javascript and images from special directory with http
 * Test your static files:
 *		$ curl -s http://localhost:8080/static/css/styles.css
 */

package main

import "net/http"

func main() {
	fs := http.FileServer(http.Dir("assets/"))
	http.Handle("/static/", http.StripPrefix("/static/", fs))

	http.ListenAndServe(":8080", nil)
}
